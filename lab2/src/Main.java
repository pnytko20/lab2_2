public class Main {

    public static double distanceToPlanet(Point3d firstPlanet, Point3d secondPlanet){
        double[] firstPlanetCoordinates = firstPlanet.getCoordinates();
        double[] secondPlanetCoordinates = secondPlanet.getCoordinates();
        return Math.sqrt((Math.pow(firstPlanetCoordinates[0] - secondPlanetCoordinates[0], 2)) + (Math.pow(firstPlanetCoordinates[1] - secondPlanetCoordinates[1], 2)) + (Math.pow(firstPlanetCoordinates[2] - secondPlanetCoordinates[2], 2)));
    }

    public static void main(String[] args) {
       double[] firstPlanetParam = {0, 0, 0};
       double[] secondPlanetParam = {20.3, 100.3, 109};

       Point3d firstPlanet = new Point3d(firstPlanetParam);
       Point3d secondPlanet = new Point3d(secondPlanetParam);

       System.out.println(distanceToPlanet(firstPlanet, secondPlanet));
    }
}
