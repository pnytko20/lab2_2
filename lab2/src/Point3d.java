public class Point3d {
    double[] coordinates;

    public Point3d(double[] coordinates){
        this.coordinates = coordinates;
    }

    public double[] getCoordinates(){
        return coordinates;
    }
}
